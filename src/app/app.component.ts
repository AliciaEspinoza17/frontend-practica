import { Component,OnInit } from '@angular/core';
import { Api1Service } from './service/api1.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  datos1: any[] = [];
  datos2: any[] = [];
  datos3: any[] = [];
  datos4: any[] = [];
  datos5: any[] = [];
  constructor (private _service:Api1Service){}
  
  ngOnInit(): void {
    this.consulta1();
    this.consulta2();
    this.consulta3();
    this.consulta4();
    this.consulta5();
    
  }

  consulta1(){
    this._service.getDatos1().subscribe((resp:any)=>{
      console.log(resp.respuesta);
      this.datos1=resp.respuesta;
    });
  }

  consulta2(){
    this._service.getDatos2().subscribe((resp:any)=>{
      console.log(resp.respuesta);
      this.datos2=resp.respuesta;
    });
  }

  consulta3(){
    this._service.getDatos3().subscribe((resp:any)=>{
      console.log(resp.respuesta);
      this.datos3=resp.respuesta;
    });
  }

  consulta4(){
    this._service.getDatos4().subscribe((resp:any)=>{
      console.log(resp.respuesta);
      this.datos4=resp.respuesta;
    });
  }

  consulta5(){
    this._service.getDatos5().subscribe((resp:any)=>{
      console.log(resp.respuesta);
      this.datos5=resp.respuesta;
    });
  }
}

