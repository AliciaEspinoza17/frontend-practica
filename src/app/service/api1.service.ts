import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})

export class Api1Service {
  public URL='http://127.0.0.1:3900'
  public url:any;
  constructor(private _http:HttpClient) { 
    this.url=this.URL;
  }
  
  public getDatos1(){
    const url=`${this.url}/obtener/producto`
    return this._http.get(url);
  }

  public getDatos2(){
    const url=`${this.url}/obtener/empleado`
    return this._http.get(url);
  }

  public getDatos3(){
    const url=`${this.url}/obtener/proovedor`
    return this._http.get(url);
  }

  public getDatos4(){
    const url=`${this.url}/obtener/stock`
    return this._http.get(url);
  }

  public getDatos5(){
    const url=`${this.url}/obtener/sucursal`
    return this._http.get(url);
  }

}
